
type inttree = Empty | Node of int * inttree * inttree

(* use this function in fromList *)
let rec insert t i =
  match t with
      Empty -> Node(i,Empty,Empty)
    | Node(j,l,r) -> 
      if i=j 
      then t 
      else if i < j 
      then Node(j,insert l i,r)
      else Node(j,l,insert r i)

(* no need for this function; it is just an example *)
let rec member t i =
  match t with
      Empty -> false
    | Node(j,l,r) -> i=j || (i < j && member l i) || member r i

(* put fromList, sum1, prod1, avg1, map and negateAll here *)
let rec fromlist a_list =
	match a_list with
	[] -> Empty
	| first::rest -> insert(fromlist rest) first

let rec sum1 a_list =
	match a_list with
	Empty -> 0
	| Node(first,l,r) -> first + sum1 l + sum1 r
let rec prod1 a_list =
	match a_list with
	Empty -> 1
	| Node(first,l,r) -> first * prod1 l * prod1 r
(*fst return the first value of the tuple, 
	snd returns the second one*)
exception DivisionByZero
let rec helper_function a_list =
	match a_list with
	Empty -> raise DivisionByZero(*because it is empty*)
	| Node(first,l,r) -> (first + fst(helper_function l) + 
	fst(helper_function r), 1 + snd(helper_function l) + 
	snd(helper_function r))
let avg1 a_list =
	fst(helper_function a_list)/snd(helper_function a_list)

	
let rec map i a_list =
	match a_list with
		Empty -> Empty
		| Node(j,l,r) -> Node(i j, map i l, map i r)

let negateAll a_list = 
	map (fun num -> 0-num) a_list

let rec fold f a t =
  match t with
      Empty -> a
    | Node(j,l,r) -> fold f (fold f (f a j) l) r

(* put sum2, prod2, and avg2 here *)
let sum2 t = fold( fun a j -> a + j) 0 t

let prod2 t = fold( fun a j -> a*j) 1 t

 

type 'a iterator = Nomore | More of 'a * (unit -> 'a iterator)

let rec iter t =
  let rec f t k =
    match t with
	Empty -> k ()
      | Node(j,l,r) -> More(j, fun () -> f l (fun () -> f r k))
  in f t (fun () -> Nomore)
  
 let rec sum3 t =
	 let rec f first k =
		 match first with
		 Nomore -> k
		 | More(x,rest) -> f (rest ()) ( k + x )
	 in f (iter t) 0

let rec prod3 t =
	let rec f it k =
		match it with
		Nomore -> k
	  | More(x,rest) -> if (x ==0) then 0
						else f (rest ()) (k * x)
	in f (iter t) 1

let avg3 t =
	let rec travel3 t =
		let rec f it k =
			match it with
			Nomore -> k
		  | More(x,rest) -> f (rest ()) (fst(k) + x, snd(k) + 1)
		in f (iter t) (0,0)
	   in let mid = travel3 t
      in fst(mid)/snd(mid)
		

(* challenge problem: put optionToException and exceptionToOption here *)

(* a little testing -- commented out since the functions do not exist yet *)


let tr = fromList [0;1;2;3;4;5;6;7;8;9;9;9;1] (* repeats get removed *)
let print_ans f t = print_string (string_of_int (f t)); print_string "\n"
let _ = print_ans sum1 tr
let _ = print_ans prod1 tr
let _ = print_ans avg1 tr
let _ = print_ans sum2 tr
let _ = print_ans prod2 tr
let _ = print_ans avg2 tr
let _ = print_ans sum3 tr
let _ = print_ans prod3 tr
let _ = print_ans avg3 tr











