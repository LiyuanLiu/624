(* 
author: Ran Tian
instructor: Boyana Norris
class: 624 Struct Prog Lang
title: homework 5 part I
*)

(* question 1*)
let addk a b f = f (a+b)
let subk a b f = f (a-b)
let timesk a b f = f (a*b)
let plusk a b f = f (a +. b) 
let take_awayk a b f = f (a -. b)
let multk a b f = f (a *. b)
let catk a b f = f (a ^ b)
let consk a b f = f ([a] @ b)
let lessk a b f = f (a < b)
let eqk a b f = f (a == b)

(* question 2 *)
let abcdk a b c d k = 
  multk b c (fun bc -> plusk a bc (fun abc -> multk d a (fun ad -> plusk abc ad k)))

(* question 3 *)
  (* i : direct style *)
let rec fact_range n m =
  if n < m then 1 
           else n * fact_range (n-1) m
  (* ii : continuation-passing style *)
let rec fact_rangek n m k =
  lessk n m (fun b -> if b then k 1
                           else subk n 1
                               (fun s -> fact_rangek s m
                                        (fun t -> timesk n t k)))

(* question 4 *)
  (* a : app_all *)
let rec app_all flst x =
  match flst with
    [] -> []
  | hd::tl -> (hd x)::(app_all tl x)
  (* b : app_allk *)
let rec app_allk flstk x k =
  match flstk with
    [] -> k []
  | hd::tl -> hd x (fun s -> app_allk tl x (fun t -> consk s t k)) 

(* question 5 *)
let rec sum_wholesk l k xk = 
  match l with
    [] -> k 0
  | hd::tl -> lessk hd 0 (fun r -> if r 
                                   then xk hd
                                   else sum_wholesk tl (fun s -> addk s hd k) xk)



(* report function for testing *)
let report x = 
  print_string "Result: ";
  print_int x;
  print_newline();; 

let reportf x = 
  print_string "Result: ";
  print_float x;
  print_newline();;

let reports x = 
  print_string "Result: ";
  print_string x;
  print_newline();;

let reportl x =
  let rec printl = 
    (function 
       [] -> ()
     | hd::tl -> print_int hd; print_string "; "; printl tl;) in
  print_string "Result: [";
  printl x;
  print_string "\b\b \b]";
  print_newline();;

let reportb x =
  print_string "Result: ";
  if x 
  then (print_string "true";print_newline())
  else (print_string "false";print_newline());;



(* test all result from questions *)
print_string "test cases:";
print_newline();

addk 3 4 report; 
subk 1 5 report;
timesk 1 5 report;
plusk 0.2 0.5 reportf;
take_awayk 0.1 0.6 reportf;
multk 0.5 0.5 reportf;
catk "Hello " "world!" reports;
consk 5 [3;4] reportl;
lessk 3 4 reportb;
eqk 4 6 reportb;
abcdk 2.0 3.0 4.0 5.0 (fun y -> report (int_of_float y));
addk 0 (fact_range 5 1) report;
fact_rangek 7 5 report;
reportl (app_all [((+) 1); (fun x -> x * x); (fun x -> 13)] 5);
reportl (app_allk [(addk 1); (fun x -> timesk x x); (fun x -> (fun k -> k 13))]
         5 (fun x -> x));
sum_wholesk [0; -1; 2; 3] report
     (fun i ->
      ( print_string ("Error: " ^ (string_of_int i) ^ " is not a whole number");
       print_newline()));;
