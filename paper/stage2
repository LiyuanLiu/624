Outline:
This paper "MultiJava:Modular Open Classes and Symmetric Multiple Dispatch for Java"
presents MultiJava, a kind of backward-compatible extension to Java supporting
open classes and symmetric multiple dispatch.

In my mind, Java has classes but when we want to add more functions to an existing class,
we need to use subclasses or edit the existing code. However, using subclasses needs we to 
pre-define it, and it will not work if existing code already creates instances of the old class.
If new subclass objects are passed to and later returned from existing code, it will require an
explicit downcasts in order to access the new operation.
The second approach is editing the existing code. It's disadvavtages are obvious: it will
break class's modularity and safety. 

Therefore, the authors provide open class and symmettic mutliple dispatch for Java.

Open class doesn't need advance planning, and it can preserve the ability to add new subclasses
modularly and safely.
Multiple dispatch retains Java's existing class-based encapsulation properties.

Contribution:
MultiJava supports both open classes and symmetric multiple dispatch while retaining Java's modular
encapsulation, typechecking, and compilation model. There is no new link-time or run-time typechecking or 
compilation needs to be performed. MultiJava extends the modular static typechecking for open classes and 
multimethods in Dubious to the much larger, more complicated, and more practical Java language. It also 
contributes a new compilation scheme for open classes and multimethods that is modular and efficient.

Papers useful:
1,Modular Statically Typed Multimethods Todd Millstein and Craig Chambers
Department of Computer Science and Engineering University of Washington {todd,chambers}@cs.washington.edu
Technical Report UW-CSE-99-03-02
March 1999
2,Erich Gamma, Richard Helm, Ralph Johnson, and John Vlissides. Design Patterns: Elements of Reusable Object-Oriented Software. Addison-Wesley, Reading, Mass., 1995.
